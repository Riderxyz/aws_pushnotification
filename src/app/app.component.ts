import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'aws-push-service';
  BarPercentage: number;
pushObj = {
  data: null as object,
  token: null as string,
  body: null as string,
  title: null as string
};
  constructor(private http: HttpClient) {

const timeToGo = timer(1000, 500).subscribe((s) => {
  console.log(s);
  this.BarPercentage = s;
  if (s >= 100) {
    this.BarPercentage = 50;
    timeToGo.unsubscribe();
  }
});

  }


  sendPush() {
    this.http.post('https://4gipivsu7b.execute-api.us-east-1.amazonaws.com/Dev/dopush', this.pushObj).subscribe((s) => {
    console.log(s);

    });
  }
}
